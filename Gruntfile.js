module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      dist: {
        src: [
          'js/build/*.js'
        ],
        dest: 'js/main.js'
      }
    },
    uglify: {
      build: {
        src: 'js/main.js',
        dest: 'js/main.min.js'
      }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'images/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'images/'
        }]
      }
    },
    sass: {
      dist: {
       files: [{
             // Set to true for recursive search
             expand: true,
             cwd: 'scss/',
             src: ['**/*.scss'],
             dest: 'css/',
             ext: '.css'
         }]
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 10 version']
      },
      multiple_files: {
        expand: true,
        flatten: true,
        src: 'css/main.css',
        dest: 'css/prefixed/'
      }
    },
    cssmin: {
      combine: {
        files: {
          'style.css': ['css/prefixed/*.css']
        }
      }
    },
    banner: '/*\n ' +
          'Theme Name:   Privacy Button\n ' +
          'Description:  Theme for the privacybutton.eu website\n ' +
          'Author:       Denis Brediceanu\n ' +
          'Author URI:   https://wpcorner.de\n ' +
          'Template:     Divi\n ' +
          'Version:      1.0\n ' +
          'Text Domain:  privacy-button \n' +
          '*/\n',
    usebanner: {
      dist: {
        options: {
          position: 'top',
          banner: '<%= banner %>'
        },
        files: {
          src: [ 'style.css' ]
        }
      }
    },

    watch: {
      options: {
        livereload: true,
      },
      scripts: {
        files: ['js/build/*.js'],
        tasks: ['concat', 'uglify']
      },
      sass: {
        files: ['scss/*.scss'],
        tasks: ['sass', 'autoprefixer', 'cssmin', 'usebanner']
      },
      images: {
        files: ['images/**/*.{png,jpg,gif}', 'images/*.{png,jpg,gif}'],
        tasks: ['imagemin']
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-banner');



  // Default task(s).
  grunt.registerTask('default', ['watch']);
};
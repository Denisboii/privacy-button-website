// add clipboard.js to <button class="clip-btn">Kopieren</button>
jQuery(document).ready(function() {
	new ClipboardJS('.clip-btn');
});

// add standard privacy button skript after contact form has been sent (for Contact Form 7 Plugin)

jQuery(document).ready(function() {
	var settingsID = 'XXXXXX';
	var btnLabel = 'Privacy Button Code kopieren';
document.addEventListener( 'wpcf7mailsent', function( event ) {
		jQuery('.wpcf7').append('<div id="standard-code"><pre><code>&lt;script type=&quot;text/javascript&quot; src=&quot;https://button.usercentrics.eu/assets/main.js&quot; id=&quot;' + settingsID +'&quot;&gt;&lt;/script&gt;</code></pre><button class="clip-btn" data-clipboard-text="' + settingsID +'">' + btnLabel +'</button></div>');
        jQuery('#standard-code').fadeIn();
    }, false );
});


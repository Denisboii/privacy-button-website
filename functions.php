<?php
function wpcorner_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); 
    wp_register_script('custom-js', get_stylesheet_directory_uri() . '/js/main.min.js', array('jquery'),'1.0', true);
    wp_enqueue_script('custom-js');
}
add_action( 'wp_enqueue_scripts', 'wpcorner_styles' );

?>